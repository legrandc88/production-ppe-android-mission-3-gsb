<?php
    function connexionPDO(){
        $login="root";
        $mdp="";
        $bd="gsb_frais";
        $serveur="localhost";
        try {
            $connexion = new PDO("mysql:host=$serveur;dbname=$bd", $login, $mdp);
            return $connexion;
        } catch (PDOException $e) {
            print "Erreur de connexion PDO ";
            die();
        }
    }
?>
