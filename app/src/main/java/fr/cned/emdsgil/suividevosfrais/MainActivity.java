package fr.cned.emdsgil.suividevosfrais;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static java.util.logging.Logger.global;


public class MainActivity extends AppCompatActivity {

    //
    private EditText Login,Pwd;
    private Button btnValider;
    private Auth auth;
    private AccesDistant accesDistant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("GSB : Authentification");
        //liaison des objets graphiques
        Login=(EditText) findViewById(R.id.txtLogin);
        Pwd=(EditText) findViewById(R.id.txtPwd);
        btnValider=(Button) findViewById(R.id.btnValider);

        //méthode évènementielle du bouton valider
        btnValider.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                executeAuthentification();
                //log.d pour savoir si l'authentification s'est déroulée correctement
                Log.d(Global.TAG,"*************************authentification déroulée correctement");

            }
        });
        }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;

    }

    public void executeAuthentification() {
        if (identificationOk(this.Login,this.Pwd)) {

            // ouvre l'activité
            Intent intent = new Intent(MainActivity.this, MenuActivity.class);
            startActivity(intent);
        } else {
            //affiche un message d'erreur
            Toast.makeText(MainActivity.this, "Echec authentification", Toast.LENGTH_SHORT).show();
            //redémarre la mainActivity
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }


    public boolean identificationOk(EditText login, EditText pwd){
        //Log d'affichage des paramètres pour déterminer si la fonction est exécutée
        Log.d(Global.TAG,"*************************************la fonction est bien appelée");
        //Création de l'objet d'authentification extrait du layout
        this.auth=new Auth(login.getText().toString(),pwd.getText().toString());
        //Log pour afficher l'objet auth après son initialisation
        Log.d(Global.TAG,"***********************************détails de l'objet auth".concat(this.auth.getLogin()).concat(" ").concat(this.auth.getMdp()));
        //envoi de auth pour récupérer les identifiants dans la base
        AccesDistant accesDistant=new AccesDistant();
        //Log pour afficher l'objet auth converti en json
        Log.d(Global.TAG,"***************************************affichage de auth converti en json".concat(this.auth.convertToJSONArray().toString()).concat("**********************"));
        accesDistant.envoi("auth",auth.convertToJSONArray());
        //récupération de l'objet Auth de la base de données************************************************************************************************
        if ((Global.login!=null) && (Global.mdp!=null)){
            Auth authbase=new Auth(Global.login, Global.mdp);

            //verification que les login et mdp issus de la base de données ont bien été extraits
            Log.d(Global.TAG,"*********************************voici le mot de passe associé au login:".concat(Global.login).concat(" ").concat(Global.mdp).concat("****************************"));
            if (auth.equals(authbase)){
                return true;
            }else {return false;}
        } else {
            Toast.makeText(MainActivity.this, "Echec accès à base de données", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
}