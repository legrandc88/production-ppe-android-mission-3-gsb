 package fr.cned.emdsgil.suividevosfrais;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

/**
 * Created by emds on 12/01/2017.
 */

public abstract class MesOutils {

    /**
     * Conversion d'une chaine au format date spécifique Android
     * @param uneDate
     * @return chaine au format date
     */
    public static Date convertStringToDate(String uneDate){
        return convertStringToDate(uneDate, "EEE MMM dd hh:mm:ss 'GMT' yyyy");
    }

    /**
     * Conversion d'une chaine au format date reçu en paramètre
     * @param uneDate
     * @param format
     * @return chaine au format date
     */
    public static Date convertStringToDate(String uneDate, String format){
        String expectedPattern = format;
        SimpleDateFormat formatter = new SimpleDateFormat(expectedPattern);
        try {
            Date date = formatter.parse(uneDate);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Conversion d'une date au format chaine
     * @param uneDate
     * @return date au format chaine
     */
    public static String convertDateToString(Date uneDate){
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return date.format(uneDate);
    }




}
