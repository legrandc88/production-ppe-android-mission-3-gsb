package fr.cned.emdsgil.suividevosfrais;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

class FraisHfAdapter extends BaseAdapter {


	private final ArrayList<FraisHf> lesFrais ; // liste des frais du mois
	private final LayoutInflater inflater ;

    /**
	 * Constructeur de l'adapter pour valoriser les propriétés
     * @param context Accès au contexte de l'application
     * @param lesFrais Liste des frais hors forfait
     */
	public FraisHfAdapter(Context context, ArrayList<FraisHf> lesFrais) {
		inflater = LayoutInflater.from(context) ;
		this.lesFrais = lesFrais ;
    }
	
	/**
	 * retourne le nombre d'éléments de la listview
	 */
	@Override
	public int getCount() {
		return lesFrais.size() ;
	}

	/**
	 * retourne l'item de la listview à un index précis
	 */
	@Override
	public Object getItem(int index) {
		return lesFrais.get(index) ;
	}

	/**
	 * retourne l'index de l'élément actuel
	 */
	@Override
	public long getItemId(int index) {
		return index;
	}

	/**
	 * structure contenant les éléments d'une ligne
	 */
	private class ViewHolder {
		TextView txtListJour ;
		TextView lblListSepare;
		TextView txtListMontant ;
		TextView lblListEuro;
		TextView txtListMotif ;
		ImageButton cmdSuppHf;

	}
	
	/**
	 * Affichage dans la liste
	 */
	@Override
	public View getView(int index, View convertView, ViewGroup parent) {
		ViewHolder holder ;
		if (convertView == null) {
			holder = new ViewHolder() ;
			convertView = inflater.inflate(R.layout.layout_liste, parent, false) ;
			//obligation de caster les convertView.findViewById?
			//ces commandes lient le holder avec les objets graphiques
			holder.txtListJour =(TextView)convertView.findViewById(R.id.txtListJour);
			holder.lblListSepare = (TextView)convertView.findViewById(R.id.lblListSepare);
			holder.txtListMontant = (TextView)convertView.findViewById(R.id.txtListMontant);
			holder.lblListEuro = (TextView)convertView.findViewById(R.id.lblListEuro);
			holder.txtListMotif = (TextView)convertView.findViewById(R.id.txtListMotif);
			holder.cmdSuppHf=(ImageButton)convertView.findViewById(R.id.cmdSuppHf);
			holder.cmdSuppHf.setImageResource(R.drawable.ic_clear_white_24dp);
			//setTag permet d'enregistrer une étiquette de holder récupérable avec getTag
			convertView.setTag(holder) ;
		}else{
			holder = (ViewHolder)convertView.getTag();
		}
		//valorisation des propriétés de holder avec les fraisHf
		holder.txtListJour.setText(String.format(Locale.FRANCE, "%d", lesFrais.get(index).getJour()));
		//rajout du lblListSepare qui fait partie de la structure de l'adapter
		holder.lblListSepare.setText(":" );
		holder.txtListMontant.setText(String.format(Locale.FRANCE, "%.2f", lesFrais.get(index).getMontant())) ;
		//rajout du lblListEuro qui fait partie de la structure de l'adapter
		holder.lblListEuro.setText("€" );
		holder.txtListMotif.setText(lesFrais.get(index).getMotif()) ;
		holder.cmdSuppHf.setTag(index);
		//méthode qui se déclenchera sur le clic du bouton suppression de l'adapter
		//On met les évènement de l'adapter avec les autres attributs
		holder.cmdSuppHf.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				int index = (Integer) v.getTag();

				//suppression du fraisHf qui se trouve à la position index de la liste adapter
				//FraisHfAdapter sera utilisée et instanciée dans HfRecapActivity
				//il n'est pas necessaire de toucher à l'objet graphique listView ici
				lesFrais.remove(index);

				//La méthode notifyDataSetChanged() doit être appelée après chaque modif de l'adapter?
				notifyDataSetChanged();
			}
		});
		return convertView ;
  	}
	
}
