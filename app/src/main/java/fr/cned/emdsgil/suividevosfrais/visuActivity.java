package fr.cned.emdsgil.suividevosfrais;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;

/**
 * créée lors de l'examen
 */
public class visuActivity extends AppCompatActivity {

    private TextView km, repas, nuitee, etape;
    private Integer bKm, bRepas, bNuitees, bEtape;
    private Integer mois, annee, key;
    private Date d;
    private String login, pwd;
    private Button fraishf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visu);
        d = new Date();
        mois = d.getMonth();
        annee = d.getYear();
        key = annee * 100 + mois;
        login = Global.login;
        pwd = Global.mdp;
        fhf_click();
        fraisForfaitsSerialises();
        valeursAffichage();
        affichage();

    }

    public void valeursAffichage() {
        this.km = (TextView) findViewById(R.id.km);
        this.repas = (TextView) findViewById(R.id.repas);
        this.nuitee = (TextView) findViewById(R.id.nuitee);
        this.etape = (TextView) findViewById(R.id.etape);
        this.fraishf = (Button) findViewById(R.id.fhf);
    }

    public void fraisForfaitsSerialises() {
        bKm = Global.listFraisMois.get(key).getKm();
        bEtape = Global.listFraisMois.get(key).getEtape();
        bNuitees = Global.listFraisMois.get(key).getNuitee();
        bRepas = Global.listFraisMois.get(key).getRepas();
    }

    public void affichage() {
        km.setText("km:" + bKm);
        repas.setText("repas: " + bRepas);
        nuitee.setText("nuitees: " + bNuitees);
        etape.setText("etape: " + bEtape);

    }

    //bouton ouvre la liste des frais hors forfait avec l'adapter, c'est donc la HfRecapActivity
    public void fhf_click() {
        fraishf.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                // ouvre l'activité
                Intent intent = new Intent(visuActivity.this, HfRecapActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;

    }
}

