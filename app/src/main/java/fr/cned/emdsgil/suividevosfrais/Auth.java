package fr.cned.emdsgil.suividevosfrais;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Leg on 30/03/2018.
 */

public class Auth {
    private String login;
    private String mdp;

    /**
     * constructeur
     *
     * @param login
     * @param mdp
     */
    public Auth(String login, String mdp) {
        this.setLogin(login);
        this.setMdp(mdp);
    }


    /**
     * getter sur login
     *
     * @return
     */
    public String getLogin() {
        return login;
    }

    /**
     * setter sur login
     *
     * @param login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * getter sur mdp
     *
     * @return
     */
    public String getMdp() {
        return mdp;
    }

    /**
     * setter sur mdp
     *
     * @param mdp
     */
    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    /**
     * Convertit les identifiants en JSONArray pour les envoyer
     * @return
     */
    public JSONArray convertToJSONArray() {
        List identifiants = new ArrayList();
        identifiants.add(this.login);
        identifiants.add(this.mdp);
        //conversion
        return new JSONArray(identifiants);
    }

}

