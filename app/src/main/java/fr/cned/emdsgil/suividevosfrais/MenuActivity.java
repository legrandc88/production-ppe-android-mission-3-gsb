package fr.cned.emdsgil.suividevosfrais;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import org.json.JSONArray;

import java.util.Hashtable;

public class MenuActivity extends AppCompatActivity {

    private AccesDistant accesDistant;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setTitle("GSB : Suivi des frais");
        // récupération des informations sérialisées
        recupSerialize();
        // chargement des méthodes événementielles
        cmdMenu_clic(((ImageButton) findViewById(R.id.cmdRepas)), FrepActivity.class);
        cmdMenu_clic(((ImageButton) findViewById(R.id.cmdNuitee)), FnuitActivity.class);
        cmdMenu_clic(((ImageButton) findViewById(R.id.cmdEtape)), FetapActivity.class);
        cmdMenu_clic(((ImageButton) findViewById(R.id.cmdKm)), KmActivity.class);
        cmdMenu_clic(((ImageButton) findViewById(R.id.cmdHf)), HfActivity.class);
        cmdMenu_clic(((ImageButton) findViewById(R.id.cmdHfRecap)), HfRecapActivity.class);
        cmdTransfert_clic();
       //rajout du bouton d'accès à la page d'authentification
        cmdMenu_clic(((ImageButton) findViewById(R.id.btnLogout)), MainActivity.class);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Récupère la sérialisation si elle existe
     */
    private void recupSerialize() {
        /* Pour éviter le warning "Unchecked cast from Object to Hash" produit par un casting direct :
         * Global.listFraisMois = (Hashtable<Integer, FraisMois>) Serializer.deSerialize(Global.filename, MenuActivity.this);
         * On créé un Hashtable générique <?,?> dans lequel on récupère l'Object retourné par la méthode deSerialize, puis
         * on cast chaque valeur dans le type attendu.
         * Seulement ensuite on affecte cet Hastable à Global.listFraisMois.
        */
        Hashtable<?, ?> monHash = (Hashtable<?, ?>) Serializer.deSerialize(MenuActivity.this);
        if (monHash != null) {
            Hashtable<Integer, FraisMois> monHashCast = new Hashtable<>();
           //On forme le monHashCast qui deviendra le Global.listFraisMois avec les données sérialisées
            for (
                    Hashtable.Entry<?, ?> entry : monHash.entrySet()) {
                monHashCast.put((Integer) entry.getKey(), (FraisMois) entry.getValue());
            }
            Global.listFraisMois = monHashCast;
        }
        // si rien n'a été récupéré, il faut créer la liste
        if (Global.listFraisMois == null) {
            Global.listFraisMois = new Hashtable<>();
            /* Retrait du type de l'HashTable (Optimisation Android Studio)
			 * Original : Typage explicit =
			 * Global.listFraisMois = new Hashtable<Integer, FraisMois>();
			*/

        }
    }

    /**
     * Sur la sélection d'un bouton dans l'activité principale ouverture de l'activité correspondante
     */
    private void cmdMenu_clic(ImageButton button, final Class classe) {
        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                // ouvre l'activité
                Intent intent = new Intent(MenuActivity.this, classe);
                startActivity(intent);
            }
        });
    }

    /**
     * Cas particulier du bouton pour le transfert d'informations vers le serveur
     */
    private void cmdTransfert_clic() {
        findViewById(R.id.cmdTransfert).setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                Hashtable<Integer,FraisMois> monHashtable=Global.listFraisMois;
                for (Hashtable.Entry<?, ?> entry :monHashtable .entrySet()) {
                    //parcours de Global.listFraisMois, récupération clés et  valeurs par entry.getKey
                    // et entry.getValue dans K et V
                    Integer K = (Integer) entry.getKey();
                    FraisMois V = (FraisMois) entry.getValue();
                    //extrait du mois et l'année de K pour les affecter à FraisMois
                    V.setMois(K%100);
                    V.setAnnee((K-V.getMois())/100);
                    //conversion de K et V au format JSON
                    JSONArray donnees = V.convertToJSONArray();
                                        //envoi des données au serveur distant
                    accesDistant.envoi("enreg",donnees);


                }


                }

            });
    }
    /**
     * Retour à l'activité principale (l'authentification)
     */
    private void retourActivityAuthentification() {
        Intent intent = new Intent(MenuActivity.this, MainActivity.class) ;
        startActivity(intent) ;
    }

}
