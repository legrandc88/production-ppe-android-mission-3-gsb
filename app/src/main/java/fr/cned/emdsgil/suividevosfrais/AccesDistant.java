package fr.cned.emdsgil.suividevosfrais;

import android.content.Context;
import android.util.Log;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by emds on 12/01/2017.
 */

public class AccesDistant implements AsyncResponse {

    // constante
    private static final String SERVERADDR = "127.0.0.1";

    /**
     * Constructeur
     */
    public AccesDistant() {

        //constructeur vide -> par défaut    }

    }
    /**
     * Traitement des informations qui viennent du serveur distant
     * @param output
     */
    @Override
    public void processFinish(String output) {
        // contenu du retour du serveur, pour contrôle dans la console
        Log.d("serveur", "************" + output);
        // découpage du message reçu
        String[] message = output.split("%");
        // contrôle si le serveur a retourné une information
        if(message.length>1){

            if(message[0].equals("enreg")){
                // retour suite à un enregistrement distant des fiches du mois
                Log.d("retour", "************enreg="+message[1]);

                //retour (du mot de passe) suite à une demande d'authentificatuon
            }else if(message[0].equals("auth")){
                Log.d("retour", "************auth="+message[1]);
                try {
                    JSONObject info = new JSONObject(message[1]);//????????????????????????????????????????????????????????????????????????????????????
                    // récupération du login de la base
                    String login=info.getString("login");
                    //récupération du mot de passe de la base
                    String mdp = info.getString("mdp");
                    //affectations au propriété statiques de la classe Global
                    Global.login=login;
                    Global.mdp=mdp;


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else if(message[0].equals("Erreur !")){
                // retour suite à une erreur
                Log.d("retour", "************erreur="+message[1]);
            }
        }

    }

    /**
     * Envoi d'informations vers le serveur distant
     * @param operation
     * @param lesDonneesJSON
     */
    public void envoi(String operation, JSONArray lesDonneesJSON){
        AccesHTTP accesDonnees = new AccesHTTP();
        // permet de faire le lien asynchrone avec AccesHTTP
        accesDonnees.delegate = this;
        // paramètres POST pour l'envoi vers le serveur distant
        accesDonnees.addParam(operation, lesDonneesJSON.toString());
        //leDonneesJSON va représenter Global.listFraisMois, ou le login

        //il aurait fallu envoyer les données uniquement si operation="lesDonnees"?
        //accesDonnees.addParam("lesDonnees", lesDonneesJSON.toString());

        //où appelle-t-on envoi? cmd_transfert classe MenuActivity
        // appel du serveur
        accesDonnees.execute(SERVERADDR);
    }
}
